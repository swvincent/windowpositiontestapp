﻿namespace WindowPositionTestApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsTextBox = new System.Windows.Forms.TextBox();
            this.openButton = new System.Windows.Forms.Button();
            this.openAsDialogButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // settingsTextBox
            // 
            this.settingsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsTextBox.Location = new System.Drawing.Point(12, 129);
            this.settingsTextBox.Name = "settingsTextBox";
            this.settingsTextBox.ReadOnly = true;
            this.settingsTextBox.Size = new System.Drawing.Size(230, 29);
            this.settingsTextBox.TabIndex = 2;
            this.settingsTextBox.TabStop = false;
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(36, 27);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(189, 23);
            this.openButton.TabIndex = 0;
            this.openButton.Text = "Open Another form";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // openAsDialogButton
            // 
            this.openAsDialogButton.Location = new System.Drawing.Point(36, 63);
            this.openAsDialogButton.Name = "openAsDialogButton";
            this.openAsDialogButton.Size = new System.Drawing.Size(189, 23);
            this.openAsDialogButton.TabIndex = 1;
            this.openAsDialogButton.Text = "Open another form as dialog";
            this.openAsDialogButton.UseVisualStyleBackColor = true;
            this.openAsDialogButton.Click += new System.EventHandler(this.openAsDialogButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "State, Left, Top, Width, Height";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 186);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.openAsDialogButton);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.settingsTextBox);
            this.MinimumSize = new System.Drawing.Size(275, 225);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Window Position";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.LocationChanged += new System.EventHandler(this.RefreshTextBox);
            this.Resize += new System.EventHandler(this.RefreshTextBox);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox settingsTextBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button openAsDialogButton;
        private System.Windows.Forms.Label label1;
    }
}

