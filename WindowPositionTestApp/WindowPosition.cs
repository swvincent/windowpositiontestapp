﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowPositionTestApp
{
    /// <summary>
    /// Save/restore window size from string.
    /// </summary>
    /// <remarks>
    /// Based on work by Jonathan Wood found at:
    /// http://www.blackbeltcoder.com/Articles/winforms/saving-and-restoring-a-forms-window-position
    /// Used under a CPOL license
    /// </remarks>
    public static class WindowPosition
    {
        public static string WindowPositionSettingsToString(this Form f)
        {
            var rect = BuildWindowBounds(f);

            return String.Format("{0},{1},{2},{3},{4}",
                (int)f.WindowState, rect.Left, rect.Top, rect.Width, rect.Height);
        }

        private static Rectangle BuildWindowBounds(Form f)
        {
            switch (f.WindowState)
            {
                case FormWindowState.Normal:
                    return f.DesktopBounds;
                case FormWindowState.Maximized:
                    return new Rectangle(f.DesktopBounds.Left + 25, f.DesktopBounds.Top + 25,
                        f.RestoreBounds.Width, f.RestoreBounds.Height);
                default:
                    return f.RestoreBounds;
            }
        }

        public static void RestoreWindowPositionSettingsFromString(this Form f, string windowPositionSettings)
        {
            if (!string.IsNullOrWhiteSpace(windowPositionSettings))
            {
                List<int> settings = ParseSettingsString(windowPositionSettings);

                if (settings.Count == 5)
                    ApplySettings(f, settings);
                else
                    throw new ArgumentException("The window position settings string is invalid",
                        "windowPositionSettings");
            }
        }

        private static List<int> ParseSettingsString(string windowPositionSettings)
        {
            var s = windowPositionSettings.Split(new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);
            return s.Select(v => int.Parse(v)).ToList();
        }

        private static void ApplySettings(Form f, List<int> settings)
        {
            f.SetBounds(settings[1], settings[2], settings[3], settings[4]);
            f.WindowState = (FormWindowState)settings[0];
        }
    }
}
