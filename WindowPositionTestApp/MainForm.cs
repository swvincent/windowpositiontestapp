﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowPositionTestApp.Properties;

namespace WindowPositionTestApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Position and size form
            string windowPosition = Settings.Default.MainFormWindowPosition;
            this.RestoreWindowPositionSettingsFromString(windowPosition);

            // Display for TS purposes
            settingsTextBox.Text = windowPosition;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Save form size and location
            Settings.Default.MainFormWindowPosition =
                this.WindowPositionSettingsToString();
            Settings.Default.Save();
        }

        private void RefreshTextBox(object sender, EventArgs e)
        {
            settingsTextBox.Text = this.WindowPositionSettingsToString();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            var f = new SomeOtherForm();
            f.Show();
        }

        private void openAsDialogButton_Click(object sender, EventArgs e)
        {
            var f = new SomeOtherForm();
            f.ShowDialog();
        }
    }
}
